import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JPanel root;
    private JButton HamburgSteakButton;
    private JButton UnadonButton;
    private JButton SpaghettiButton;
    private JTextPane OrderedItemsBox;
    private JButton SaladButton;
    private JButton CakeButton;
    private JButton DrinkBarButton;
    private JButton CheckOutButton;
    private JTextPane YenBox;
    private JLabel OrderedItems;
    private JLabel HamburgSteakLabel;
    private JLabel SpaghettiLabel;
    private JLabel UnadonLabel;
    private JLabel CakeLabel;
    private JLabel DrinkBarLabel;
    int total = 0;

    void addText(String food, int cost, String currentText, String addWords){
        OrderedItemsBox.setText(currentText + food + addWords + "   " + cost + " yen\n\n");
    }
    void hamburgSteak(String food, int cost, String currentText){
        String selectValues[] = {"Demi-glace source", "Garlic source"};
        int sourceSelect = JOptionPane.showOptionDialog(null,"Which source would you like?",
                "Source selection",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,null,selectValues,
                selectValues[0]);
        if (sourceSelect == 0){
            String addWords = "(Demi-glace source)";
            addText(food, cost, currentText, addWords);
        }
        if(sourceSelect == 1){
            String addWords = "(Garlic source)";
            addText(food, cost, currentText, addWords);
        }
    }
    void extraLarge(String food, int cost, String currentText){
        String selectValues[] = {"Extra-large portion", "Normal portion"};
        int portionSelect = JOptionPane.showOptionDialog(null,"Would you like to pay an " +
                        "additional 50 yen to get an \"Extra-large portion\"?", "Portion",
                JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,null,selectValues, selectValues[0]);
        if (portionSelect == 0){
            total +=  50;
            String addWords = "(Extra-large portion)";
            cost += 50;
            addText(food, cost, currentText, addWords);
        }
        if(portionSelect == 1){
            String addWords = "(Normal portion)";
            addText(food, cost, currentText, addWords);
        }
    }
    void dessertTime(String food, int cost, String currentText){
        String selectValues[] = {"Before the meal", "After the meal"};
        int timeSelect = JOptionPane.showOptionDialog(null,"When would you like to have your " +
                        "dessert?", "Dessert time",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,
                null,selectValues, selectValues[0]);
        if (timeSelect == 0){
            String addWords = "(Before the meal)";
            addText(food, cost, currentText, addWords);
        }
        if(timeSelect == 1){
            String addWords = "(After the meal)";
            addText(food, cost, currentText, addWords);
        }
    }
    void drinkBar(String currentText){
        int drinkbarConfirmation =  JOptionPane.showConfirmDialog(null,
                "Would you like to pay an additional 150 yen to use \"Set Drink Bar\"?",
                "Recommendation of Set Menu",
                JOptionPane.YES_NO_OPTION);
        if(drinkbarConfirmation == 0){
            currentText= currentText.substring(0, currentText.length() - 1);
            OrderedItemsBox.setText(currentText + "(+Set Drink Bar  150 yen)\n\n");
            total += 150;
        }
    }
    void order(String food, int cost, int num){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0){
            total += cost;
            String currentText = OrderedItemsBox.getText();
            switch(num){
                case 1:
                    hamburgSteak(food, cost, currentText);
                    currentText = OrderedItemsBox.getText();
                    drinkBar(currentText);
                    break;
                case 2:
                case 3:
                    extraLarge(food, cost, currentText);
                    currentText = OrderedItemsBox.getText();
                    drinkBar(currentText);
                    break;
                case 4:
                    addText(food, cost, currentText, "");
                    currentText = OrderedItemsBox.getText();
                    drinkBar(currentText);
                    break;
                case 5:
                    dessertTime(food, cost, currentText);
                    currentText = OrderedItemsBox.getText();
                    drinkBar(currentText);
                    break;
                case 6:
                    addText(food, cost, currentText, "");
                default:
                    break;
            }
            currentText = OrderedItemsBox.getText();
            YenBox.setText("Total   " + total + " yen");
            JOptionPane.showMessageDialog(null, "Order for " + food + " received.");
        }
    }

    public FoodGUI() {
        YenBox.setText("Total  " + total + " yen");
        HamburgSteakButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Hamburg Steak", 1000, 1);
            }
        });
        HamburgSteakButton.setIcon(new ImageIcon(
                this.getClass().getResource("HamburgSteak.jpg")
        ));
        SpaghettiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Spaghetti", 600, 2);
            }
        });
        SpaghettiButton.setIcon(new ImageIcon(
                this.getClass().getResource("Spaghetti.jpg")
        ));
        UnadonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Unadon", 1200, 3);
            }
        });
        UnadonButton.setIcon(new ImageIcon(
                this.getClass().getResource("Unadon.jpg")
        ));
        SaladButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Shrimp And Squid Salad", 900, 4);
            }
        });
        SaladButton.setIcon(new ImageIcon(
                this.getClass().getResource("ShrimpAndSquidSalad.jpg")
        ));
        CakeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Strawberry Cheseccake", 400, 5);
            }
        });
        CakeButton.setIcon(new ImageIcon(
                this.getClass().getResource("StrawberryCheseccake.jpg")
        ));
        DrinkBarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Drink Bar", 350, 6);
            }
        });
        DrinkBarButton.setIcon(new ImageIcon(
                this.getClass().getResource("DrinkBar.jpg")
        ));
        CheckOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int checkout = JOptionPane.showConfirmDialog(null,
                        "Would you like to check out?", "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(checkout == 0){
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is  "+
                            total +" yen.");
                    String currentText = OrderedItemsBox.getText();
                    currentText = null;
                    OrderedItemsBox.setText(currentText);
                    total = 0;
                    YenBox.setText("Total  " + total + " yen");
                }
            }
        });
    }

        public static void main (String[]args){
            JFrame frame = new JFrame("FoodGUI");
            frame.setContentPane(new FoodGUI().root);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.pack();
            frame.setVisible(true);
        }
    }


